#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include "disk.h"


char *month_to_string(short m) {
    switch(m) {
    case 1: return "Jan";
    case 2: return "Feb";
    case 3: return "Mar";
    case 4: return "Apr";
    case 5: return "May";
    case 6: return "Jun";
    case 7: return "Jul";
    case 8: return "Aug";
    case 9: return "Sep";
    case 10: return "Oct";
    case 11: return "Nov";
    case 12: return "Dec";
    default: return "?!?";
    }
}


void unpack_datetime(unsigned char *time, short *year, short *month, 
    short *day, short *hour, short *minute, short *second)
{
    assert(time != NULL);

    memcpy(year, time, 2);
    *year = htons(*year);

    *month = (unsigned short)(time[2]);
    *day = (unsigned short)(time[3]);
    *hour = (unsigned short)(time[4]);
    *minute = (unsigned short)(time[5]);
    *second = (unsigned short)(time[6]);
}


int main(int argc, char *argv[]) {
    superblock_entry_t sb;
    directory_entry_t de;
    int  i;
    char *imagename = NULL;
    FILE *f;
    short year;
    short month;
    short day;
    short hour;
    short minute;
    short second;

    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--image") == 0 && i+1 < argc) {
            imagename = argv[i+1];
            i++;
        }
    }

    if (imagename == NULL)
    {
        fprintf(stderr, "usage: lsuvfs --image <imagename>\n");
        exit(1);
    }

    f = fopen(imagename, "r");
    
    fread(sb.magic, 8, 1, f);
    
    fread(&sb.block_size, 2, 1, f);
    sb.block_size = htobe16(sb.block_size);

    fread(&sb.num_blocks, 4, 1, f);
    sb.num_blocks = htobe32(sb.num_blocks);

    fread(&sb.fat_start, 4, 1, f);
    sb.fat_start = htobe32(sb.fat_start);

    fread(&sb.fat_blocks, 4, 1, f);
    sb.fat_blocks = htobe32(sb.fat_blocks);

    fread(&sb.dir_start, 4, 1, f);
    sb.dir_start = htobe32(sb.dir_start);

    fread(&sb.dir_blocks, 4, 1, f);
    sb.dir_blocks = htobe32(sb.dir_blocks);

    fseek(f, (sb.dir_start * sb.block_size), SEEK_SET);

    for(i = 0; i < (sb.dir_blocks * sb.block_size)/64; i++) {
        
        fread(&de.status, 1, 1, f);

        fread(&de.start_block, 4, 1, f);
        de.start_block = htobe32(de.start_block);

        fread(&de.num_blocks, 4, 1, f);
        de.num_blocks = htobe32(de.num_blocks);

        fread(&de.file_size, 4, 1, f);
        de.file_size = htobe32(de.file_size);

        fread(de.create_time, 7, 1, f);
        unpack_datetime(de.create_time, &year, &month, &day, &hour, &minute, &second);

        fread(de.modify_time, 7, 1, f);
        unpack_datetime(de.modify_time, &year, &month, &day, &hour, &minute, &second);

        fread(de.filename, 31, 1, f);

        fread(de._padding, 6, 1, f);

        if(de.status == 0) {
            continue;
        }
        printf("status: %d\n", de.status);
        printf("start block: %d\n", de.start_block);
        printf("num blocks: %d\n", de.num_blocks);
        printf("file size: %d\n", de.file_size);
        printf("create time: %hu %hu %hu %hu %hu %hu\n", year, month, day, hour, minute, second);
        printf("modify time: %hu %hu %hu %hu %hu %hu\n", year, month, day, hour, minute, second);
        printf("file name: %s\n", de.filename);
    }

    fclose(f);

    return 0; 
}
