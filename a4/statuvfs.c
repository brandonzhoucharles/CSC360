#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include "disk.h"

int main(int argc, char *argv[]) {
    superblock_entry_t sb;
    int  i;
    char *imagename = NULL;
    FILE  *f;
    int   *fat_data;
    int unused = 0;
    int reserved = 0;
    int used = 0;

    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--image") == 0 && i+1 < argc) {
            imagename = argv[i+1];
        }
    }

    if (imagename == NULL)
    {
        fprintf(stderr, "usage: statuvfs --image <imagename>\n");
        exit(1);
    }

    f = fopen(imagename, "r");
    fread(sb.magic, 8, 1, f);
    
    fread(&sb.block_size, 2, 1, f);
    sb.block_size = htobe16(sb.block_size);

    fread(&sb.num_blocks, 4, 1, f);
    sb.num_blocks = htobe32(sb.num_blocks);

    fread(&sb.fat_start, 4, 1, f);
    sb.fat_start = htobe32(sb.fat_start);

    fread(&sb.fat_blocks, 4, 1, f);
    sb.fat_blocks = htobe32(sb.fat_blocks);

    fread(&sb.dir_start, 4, 1, f);
    sb.dir_start = htobe32(sb.dir_start);

    fread(&sb.dir_blocks, 4, 1, f);
    sb.dir_blocks = htobe32(sb.dir_blocks);

    fseek(f, (sb.fat_start * sb.block_size), SEEK_SET);

    fat_data = malloc(sb.num_blocks * 4);

    fread(fat_data, sb.num_blocks * 4, 1, f);
    
    for(i = 0; i < sb.num_blocks; i++) {
        if(htobe32(fat_data[i]) == 0x00000000) {
            unused++;
        } else if(htobe32(fat_data[i]) == 0x00000001) {
            reserved++;
        } else {
            used++;
        }
    }    
    
    fclose(f);
    
    printf("magic: %s\n", sb.magic);
    printf("block size: %d\n", sb.block_size);
    printf("num blocks: %d\n", sb.num_blocks);
    printf("fat start: %d\n", sb.fat_start);
    printf("fat blocks: %d\n", sb.fat_blocks);
    printf("dir start: %d\n", sb.dir_start);
    printf("dir blocks: %d\n", sb.dir_blocks);
    printf("free: %d\n", unused);
    printf("reserved: %d\n", reserved);
    printf("used: %d\n", used);

    return 0; 
}
