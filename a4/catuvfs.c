#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include "disk.h"


int main(int argc, char *argv[]) {
    superblock_entry_t sb;
    directory_entry_t de;

    int  i;
    char *imagename = NULL;
    char *filename  = NULL;
    FILE *f;
    char *filedata = NULL;
    int  *fat_data;
    int  stored_value;

    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--image") == 0 && i+1 < argc) {
            imagename = argv[i+1];
            i++;
        } else if (strcmp(argv[i], "--file") == 0 && i+1 < argc) {
            filename = argv[i+1];
            i++;
        }
    }

    if (imagename == NULL || filename == NULL) {
        fprintf(stderr, "usage: catuvfs --image <imagename> " \
            "--file <filename in image>");
        exit(1);
    }

    f = fopen(imagename, "r");
    
    fread(sb.magic, 8, 1, f);
    
    fread(&sb.block_size, 2, 1, f);
    sb.block_size = htobe16(sb.block_size);

    fread(&sb.num_blocks, 4, 1, f);
    sb.num_blocks = htobe32(sb.num_blocks);

    fread(&sb.fat_start, 4, 1, f);
    sb.fat_start = htobe32(sb.fat_start);

    fread(&sb.fat_blocks, 4, 1, f);
    sb.fat_blocks = htobe32(sb.fat_blocks);

    fread(&sb.dir_start, 4, 1, f);
    sb.dir_start = htobe32(sb.dir_start);

    fread(&sb.dir_blocks, 4, 1, f);
    sb.dir_blocks = htobe32(sb.dir_blocks);

    fseek(f, (sb.dir_start * sb.block_size), SEEK_SET);

    for(i = 0; i < (sb.dir_blocks * sb.block_size)/64; i++) {
        
        fread(&de.status, 1, 1, f);

        fread(&de.start_block, 4, 1, f);
        de.start_block = htobe32(de.start_block);

        fread(&de.num_blocks, 4, 1, f);
        de.num_blocks = htobe32(de.num_blocks);

        fread(&de.file_size, 4, 1, f);
        de.file_size = htobe32(de.file_size);

        fread(de.create_time, 7, 1, f);

        fread(de.modify_time, 7, 1, f);

        fread(de.filename, 31, 1, f);

        fread(de._padding, 6, 1, f);

        if(de.status == 0) {
            continue;
        }

        if(strcmp(de.filename, filename) == 0) {
            fseek(f, (sb.fat_start * sb.block_size), SEEK_SET);
            
            fat_data = malloc(sb.num_blocks * 4);

            fread(fat_data, sb.num_blocks * 4, 1, f);

            fseek(f, (de.start_block * sb.block_size), SEEK_SET);

            filedata = malloc(sb.block_size);

            stored_value = htobe32(fat_data[de.start_block]);

            for(i = 0; i < de.file_size; i += sb.block_size) {
                fread(filedata, sb.block_size, 1, f);
                printf("%s", filedata);
                fseek(f, (htobe32(fat_data[stored_value]) * sb.block_size), SEEK_SET);
                stored_value = htobe32(fat_data[stored_value]);
            }
            break;
        }
    }

    fclose(f);

    return 0; 
}
