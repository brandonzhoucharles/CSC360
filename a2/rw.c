/*Required Headers*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include "rw.h"
#include "resource.h"

/*
 * Declarations for reader-writer shared variables -- plus concurrency-control
 * variables -- must START here.
 */

pthread_mutex_t m;
pthread_cond_t readersQ;
pthread_cond_t writersQ;
int readers;
int writers;
int active_writers;
int status;

static resource_t data;

void initialize_readers_writer() {
    /*
     * Initialize the shared structures, including those used for
     * synchronization.
     */
    
    /* These lines run only once at solution's start. */
    readers = 0;
    writers = 0;
    active_writers = 0;

    init_resource(&data, "data");
    
    // mutex init
    status = pthread_mutex_init(&m, NULL);
    if (status != 0) {
        fprintf(stderr, "Error creating mutex'm'\n");
        exit(1);
    }

    // reader init
    status = pthread_cond_init(&readersQ,  NULL);
    if (status != 0) {
        fprintf(stderr, "Could not initialize 'condition'\n");
        exit(1);
    }

    // writer init
    status = pthread_cond_init(&writersQ,  NULL);
    if (status != 0) {
        fprintf(stderr, "Could not initialize 'condition'\n");
        exit(1);
    }
}

void rw_read(char *value, int len) {
    pthread_mutex_lock(&m);
    while (!(writers == 0)) {
        pthread_cond_wait(&readersQ, &m);
    }
    readers++;
    pthread_mutex_unlock(&m);
    
    /* read */
    read_resource(&data, value, len);

    pthread_mutex_lock(&m);
    if (--readers == 0) {
        pthread_cond_signal(&writersQ);
    }
    pthread_mutex_unlock(&m);
}

void rw_write(char *value, int len) {
    pthread_mutex_lock(&m);
    writers++;

    while (!((readers == 0) && (active_writers== 0))) {
        pthread_cond_wait(&writersQ, &m);
    }
    
    active_writers++;
    pthread_mutex_unlock(&m);
    
    /* write */
    write_resource(&data, value, len);

    pthread_mutex_lock(&m);
    writers--;
    active_writers--;
    
    if (writers) {
        pthread_cond_signal(&writersQ);
    } else {
        pthread_cond_broadcast(&readersQ);
    }
    pthread_mutex_unlock(&m);
}