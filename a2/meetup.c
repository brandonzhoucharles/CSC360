/*Required Headers*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include "meetup.h"
#include "resource.h"

// int pthread_barrier_destroy(pthread_barrier_t *barrier);
// int pthread_barrier_wait(pthread_barrier_t *barrier);

/*
 * Declarations for barrier shared variables -- plus concurrency-control
 * variables -- must START here.
 */

pthread_barrier_t barrier;
sem_t semaphore_group;
sem_t semaphore;
sem_t semaphore_incr;

static resource_t data;
int count;
int status;
int meet;
int num_people;
int generation;
int incrementer;

void initialize_meetup(int n, int mf) {
    char label[100];
    int i;

    meet = mf;
    num_people = n;
    status = 0;
    count = 0;
    generation = 0;
    incrementer = 0;

    init_resource(&data, "data");

    if (n < 1) {
        fprintf(stderr, "Who are you kidding?\n");
        fprintf(stderr, "A meetup size of %d??\n", n);
        exit(1);
    }

    /*
     * Initialize the shared structures, including those used for
     * synchronization.
     */

    status = sem_init(&semaphore, 0, 1);
    if (status != 0) {
        fprintf(stderr, "Error creating semaphore 'semaphore'\n");
        exit(1);
    }

    status = sem_init(&semaphore_group, 0, n);
    if (status != 0) {
        fprintf(stderr, "Error creating semaphore 'semaphore'\n");
        exit(1);
    }
    
    status = sem_init(&semaphore_incr, 0, n);
    if (status != 0) {
        fprintf(stderr, "Error creating semaphore 'semaphore'\n");
        exit(1);
    }
    
    status = pthread_barrier_init(&barrier, 0, n);
    if (status != 0) {
        fprintf(stderr, "Error creating barrier 'barrier'\n");
        exit(1);
    }
}

void join_meetup(char *value, int len) {
    
    // generation check
    sem_wait(&semaphore_incr);
    int my_generation = incrementer / num_people;
    incrementer++;
    sem_post(&semaphore_incr);
    
    sem_wait(&semaphore_group);

    while (my_generation != generation) {
        sem_post(&semaphore_group);
        sem_wait(&semaphore_group);
    }

    sem_wait(&semaphore);
    if((meet == 1) && (count == 0)) {
        write_resource(&data, value, len);
    } else if((meet == 0) && (count == (num_people - 1))) {
        write_resource(&data, value, len);
    }
    count++;

    if(count == num_people) {
        count = 0;
    }
    sem_post(&semaphore);

    if(pthread_barrier_wait(&barrier) != 0) {
        generation++;
    }
    read_resource(&data, value, len);
    sem_post(&semaphore_group);
}
